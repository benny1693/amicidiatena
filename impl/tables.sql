CREATE TABLE Personaggio(
	Codice CHAR(4) PRIMARY KEY,
	Nome VARCHAR(20) NOT NULL,
	Sesso ENUM('F','M') NOT NULL
);

CREATE TABLE Genitorialita(
	Genitore CHAR(4),
	Figlio CHAR(4),
	PRIMARY KEY (Genitore,Figlio),
	
	FOREIGN KEY (Genitore) REFERENCES Personaggio(Codice) 
				ON UPDATE CASCADE
				ON DELETE CASCADE,
	FOREIGN KEY (Figlio) REFERENCES Personaggio(Codice) 
				ON UPDATE CASCADE
				ON DELETE CASCADE 
);

CREATE TABLE FiguraMitologica (
    Codice CHAR(4) PRIMARY KEY,
    Tipo VARCHAR(17) NOT NULL,
    
    FOREIGN KEY (Codice) REFERENCES Personaggio (Codice) 
    			ON UPDATE CASCADE 
    			ON DELETE CASCADE
);

CREATE TABLE Creatura(
	Codice CHAR(4) PRIMARY KEY,
	
	FOREIGN KEY (Codice) REFERENCES FiguraMitologica(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE AnimaleIspirazione(
	CodiceCreatura CHAR(4),
	Nome VARCHAR(25),
	PRIMARY KEY (CodiceCreatura, Nome),
	
	FOREIGN KEY (CodiceCreatura) REFERENCES Creatura(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE Divinita(
	Codice CHAR(4) PRIMARY KEY,
	Categoria VARCHAR(10) NOT NULL,
	Campo VARCHAR(40),
	
	FOREIGN KEY (Codice) REFERENCES FiguraMitologica(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE EssereUmano(
	Codice CHAR(4) PRIMARY KEY,
	Tipo CHAR(8) NOT NULL,
	AnnoNascita SMALLINT,
	
	FOREIGN KEY (Codice) REFERENCES Personaggio(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE Epiteto(
	Appellativo VARCHAR(40) PRIMARY KEY,
	CodicePersonaggio CHAR(4) NOT NULL,
	
	FOREIGN KEY (CodicePersonaggio) REFERENCES Personaggio(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE RapprModerna(
	Codice CHAR(3) PRIMARY KEY,
	Titolo VARCHAR(40) NOT NULL,
	AutoreModerno VARCHAR(25) NOT NULL,
	Nazionalita VARCHAR(11),
	Data SMALLINT,
	Tipo VARCHAR(12) NOT NULL,
	UNIQUE(Titolo,AutoreModerno)
);

CREATE TABLE RapprArtistica(
	Codice CHAR(3) PRIMARY KEY,
	TipologiaArte VARCHAR(15),
	
	FOREIGN KEY (Codice) REFERENCES RapprModerna(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE Luogo(
	Codice CHAR(4) PRIMARY KEY,
	Nome VARCHAR(40),
	Tipo VARCHAR(12) NOT NULL
);

CREATE TABLE LuogoReale(
	CodiceLuogo CHAR(4) PRIMARY KEY,
	RegioneGeografica VARCHAR(20) NOT NULL,
	
	FOREIGN KEY (CodiceLuogo) REFERENCES Luogo(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE Evento(
	Codice CHAR(4) PRIMARY KEY,
	Nome VARCHAR(40) NOT NULL,
	Epoca VARCHAR(12) NOT NULL,
	NomeGreco VARCHAR(50),
	Descrizione TEXT,
	CodiceLuogo CHAR(4) NOT NULL,
	UNIQUE(Nome, Epoca),
	FOREIGN KEY (CodiceLuogo) REFERENCES Luogo(Codice)
		ON UPDATE NO ACTION
		ON DELETE NO ACTION
);

CREATE TABLE SitoArcheologico(
	Codice CHAR (3),
	Nome VARCHAR(40),
	Ente VARCHAR(40),
	CodiceLuogo CHAR(4) UNIQUE NOT NULL,

	PRIMARY KEY (Codice),
	FOREIGN KEY (CodiceLuogo) REFERENCES LuogoReale(CodiceLuogo)
				ON UPDATE NO ACTION
				ON DELETE NO ACTION
);

CREATE TABLE Autore(
	Codice CHAR(4) PRIMARY KEY,
	Nome VARCHAR(25) NOT NULL,
	CittaOrigine VARCHAR(20),
	CittaDecesso VARCHAR(20),
	Periodo TINYINT,
	Tipo VARCHAR(9)
);

CREATE TABLE Fonte(
	Codice CHAR(5) PRIMARY KEY,
	Titolo VARCHAR(30) NOT NULL,
	CodiceAutore CHAR(4) NOT NULL,
	Data SMALLINT,
	Locazione VARCHAR(50),
	UNIQUE(Titolo, CodiceAutore),

	FOREIGN KEY(CodiceAutore) REFERENCES Autore(Codice)
				ON UPDATE CASCADE
				ON DELETE NO ACTION
);

CREATE TABLE FonteScritta(
	Codice CHAR(5) PRIMARY KEY,
	Provenienza VARCHAR(10),
	
	FOREIGN KEY (Codice) REFERENCES Fonte(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE FonteArcheologica(
	Codice CHAR(5) PRIMARY KEY,
	Tipo VARCHAR(25),
	CodiceSito CHAR(3) NOT NULL,

	FOREIGN KEY (Codice) REFERENCES Fonte(Codice)
				ON UPDATE CASCADE 
				ON DELETE CASCADE,
	FOREIGN KEY (CodiceSito) REFERENCES SitoArcheologico(Codice)
				ON UPDATE CASCADE
				ON DELETE CASCADE
);

CREATE TABLE Coinvolgimento(
	CodicePersonaggio CHAR(4),
	CodiceEvento CHAR(4),
	PRIMARY KEY(CodicePersonaggio, CodiceEvento),
	
	FOREIGN KEY(CodicePersonaggio) REFERENCES Personaggio(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE,
	FOREIGN KEY(CodiceEvento) REFERENCES Evento(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE Ispirazione(
	CodiceRapp CHAR(3),
	CodicePersonaggio CHAR(4),

	PRIMARY KEY(CodiceRapp, CodicePersonaggio),
	
	FOREIGN KEY (CodiceRapp) REFERENCES RapprModerna(Codice)
				ON UPDATE CASCADE 
				ON DELETE CASCADE,
	FOREIGN KEY (CodicePersonaggio) REFERENCES Personaggio(Codice)
				ON UPDATE CASCADE 
				ON DELETE CASCADE
);

CREATE TABLE Narrazione(
	CodiceFonte CHAR(5),
	CodiceEvento CHAR(4),
	PRIMARY KEY(CodiceFonte, CodiceEvento),
	
	FOREIGN KEY(CodiceFonte) REFERENCES Fonte(Codice)
				ON UPDATE NO ACTION 
				ON DELETE NO ACTION,
	FOREIGN KEY(CodiceEvento) REFERENCES Evento(Codice)
				ON UPDATE CASCADE
				ON DELETE CASCADE
);

CREATE TABLE Delineazione(
	CodicePersonaggio CHAR(4),
	CodiceFonte CHAR(5),
	PRIMARY KEY (CodicePersonaggio, CodiceFonte),
	
	FOREIGN KEY (CodicePersonaggio) REFERENCES Personaggio(Codice) 
				ON UPDATE CASCADE 
				ON DELETE CASCADE,
	FOREIGN KEY (CodiceFonte) REFERENCES Fonte(Codice) 
				ON UPDATE NO ACTION
				ON DELETE NO ACTION
);
