/* OPERAZIONI */
/* Viste supplementari */
CREATE VIEW FigliDiCoppie AS
	-- Mostra i figli che hanno due genitori e i loro codici
	SELECT DISTINCT G1.Figlio, G1.Genitore AS Genitore1, 
					G2.Genitore AS Genitore2
	FROM Genitorialita AS G1, Genitorialita AS G2
	WHERE G1.Figlio = G2.Figlio AND G1.Genitore < G2.Genitore;

CREATE VIEW EventiInLuoghiMitologici AS
	-- Mostra tutti gli eventi avvenuti in luoghi mitologici e la loro epoca
	SELECT DISTINCT E.Codice, E.Epoca
	FROM Evento AS E, Luogo AS L
	WHERE E.CodiceLuogo = L.Codice AND L.Tipo = 'Mitologico';

CREATE VIEW DivinitaEpiteti AS
	-- Mostra tutte le divinita con i loro epiteti
	SELECT P.Codice, P.Nome, D.Campo, D.Categoria, E.Appellativo
	FROM Personaggio AS P, Epiteto AS E, Divinita AS D
	WHERE P.Codice = E.CodicePersonaggio AND P.Codice = D.Codice;

/* Operazione 1 */
CREATE VIEW FilmEroi AS
/* stampa il nome, l'autore e la nazionalità di tutti i film che hanno tra i 
personaggi semidivinità o eroi */
	SELECT	DISTINCT RM.Titolo AS TitoloFilm, RM.AutoreModerno AS Regista, 
			RM.Nazionalita AS Nazionalita
	FROM (	FiguraMitologica AS FM 
				JOIN 
			Ispirazione AS I ON FM.Codice = I.CodicePersonaggio)
				JOIN
			RapprModerna AS RM ON RM.Codice = I.CodiceRapp
	WHERE FM.Tipo = 'Semidivinita/Eroe' AND RM.Tipo = 'Film';
			
/* Operazione 2 */
CREATE VIEW FontiLuogo AS
/* stampa titolo, tipo e nome dell'autore di tutte le fonti archeologiche 
rinvenute nello stesso luogo dell’evento narrato */
	SELECT	F.Titolo AS Titolo, FA.Tipo AS Tipo, A.Nome AS Autore
	FROM ((((FonteArcheologica AS FA 
				JOIN
			Narrazione AS N ON FA.Codice = N.CodiceFonte)
				JOIN
			Evento AS E ON N.CodiceEvento = E.Codice)
				JOIN
			SitoArcheologico AS S ON FA.CodiceSito = S.Codice) 
			-- fin qui vengono trovate tutte le fonti archeologiche con il 
			-- relativo sito di ritrovamento ed evento narrato
				JOIN
			Fonte AS F ON FA.Codice = F.Codice)
				JOIN
			Autore AS A ON F.CodiceAutore = A.Codice 
			-- vengono ottenute le informazioni della fonte e dell'autore
	WHERE E.CodiceLuogo = S.CodiceLuogo;

/* Operazione 6 */
CREATE VIEW LuoghiCollegati_A AS
/* stampa il codice e il nome di tutti i luoghi che sono collegati sia a un sito 
archeologico (il cui nome inizia con ’A’) che a un evento, non avvenuto 
nell’epoca degli eroi, di cui non si conosce il nome in lingua greca */
	SELECT Codice, Nome
	FROM Luogo AS L
	WHERE Codice IN (	-- tutti i luoghi legati a un sito archeologico con D 
						-- come iniziale del nome
						SELECT CodiceLuogo  
						FROM SitoArcheologico 
						WHERE Nome LIKE 'A%')
			AND
		  Codice IN (	-- tutti i luoghi legati a un evento di cui si conosce 
		  				-- il nome greco e che non sono avvenuti nell'epoca 
		  				-- degli eroi
		  				SELECT CodiceLuogo 
		  				FROM Evento
		  				WHERE NomeGreco IS NULL AND Epoca <> 'Eroi');


/* Operazione 7 */
CREATE VIEW StampaEventi as 
/* stampa nome ed epoca di tutti gli eventi che coinvolgono almeno due personaggi e sono narrati da massimo tre fonti. */
	SELECT DISTINCT E.Codice, E.Nome, E.Epoca
	FROM Evento as E JOIN Coinvolgimento as C ON E.Codice = C.CodiceEvento,
		 Narrazione as N
	WHERE N.CodiceEvento = E.Codice
	GROUP BY E.Codice, E.Nome, E.Epoca, E.Descrizione
	HAVING count(distinct C.CodicePersonaggio)>=2 
			AND count(distinct N.CodiceFonte)<=3;


/* Operazione 8 */
CREATE VIEW CittaAutoriAreaGeo AS
/* stampa le città di origine di tutti gli autori, vissuti in secoli precedenti a
II a.C., che hanno prodotto fonti archeologiche rinvenute in siti che si trovano 
in Attica */
	SELECT DISTINCT A.CittaOrigine
	FROM (Autore AS A
			JOIN
		Fonte AS F ON F.CodiceAutore = A.Codice)
			JOIN
		FonteArcheologica AS FA ON F.Codice = FA.Codice
	WHERE A.Periodo < -2 AND
		FA.CodiceSito IN (	-- i codici di tutti i siti che si trovano in Attica
							SELECT S.Codice
							FROM SitoArcheologico AS S 
									JOIN
								LuogoReale AS LR ON S.CodiceLuogo=LR.CodiceLuogo
							WHERE LR.RegioneGeografica = 'Attica');

/* Operazione 9 */
CREATE VIEW StampaDivinita as
/* stampa codice e nome di tutte le divinità , con tutti i loro appellativi e il loro 
campo, che hanno entrambi i genitori e che siano coinvolti in eventi di al più 
due epoche, avvenuti in un luogo mitologico */
	SELECT DE.Codice, DE.Nome, DE.Campo, DE.Appellativo
	FROM ((	DivinitaEpiteti as DE
					JOIN 
			Coinvolgimento as CO ON CO.CodicePersonaggio=DE.Codice)
					JOIN 
			EventiInLuoghiMitologici as EM ON CO.CodiceEvento = EM.Codice)
					JOIN
			FigliDiCoppie as FC ON CO.CodicePersonaggio = FC.Figlio
	GROUP BY DE.Codice, DE.Nome, DE.Campo, DE.Appellativo
	HAVING count(distinct EM.Epoca)<=2 ;

/* Operazione 19 */
CREATE VIEW AnimaleFiglio as
/* stampa il nome e gli animali di ispirazione delle creature che sono figlie di 
almeno una divinità ordinati per codice e nome */
	SELECT CR.Codice AS Codice_Creatura, PE.Nome AS Nome, AI.Nome AS Animale
	FROM Creatura as CR, AnimaleIspirazione as AI, Personaggio as PE, 
			Genitorialita as GE, Divinita as DI
	WHERE CR.Codice=AI.CodiceCreatura AND CR.Codice=PE.Codice 
			AND GE.Figlio=PE.Codice AND GE.Genitore=DI.Codice
	ORDER BY CR.Codice, PE.Nome;
