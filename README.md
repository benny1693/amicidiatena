# Amici di Atena
## Progetto di Basi di Dati (A.A. 2017/2018)

Il progetto si propone di modellare tramite un database le relazioni che intercorrono tra i personaggi, gli eventi, i luoghi e le fonti della mitologia greca.
Gli sviluppatori sono:
- Laura Cameran
- Benedetto Cosentino
